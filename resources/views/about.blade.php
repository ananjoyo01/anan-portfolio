<!DOCTYPE HTML>
<html>
<head>
<title>Anan Portfolio</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
 <!-- Custom Theme files -->
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="keywords" content="Preface Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<!-- webfonts -->
	<link href='//fonts.googleapis.com/css?family=Asap:400,700,400italic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
<!-- webfonts -->
 <!---- start-smoth-scrolling---->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
 <!---- start-smoth-scrolling---->
</head>
<body>
<!-- about -->
			<div id="about" class="about">
				<div class="col-md-6 about-left">
					<div id="owl-demo1" class="owl-carousel owl-carousel2">
					                <div class="item">
					                	<div class="about-left-grid">
											<h2>Hi! Saya <span>Adi Hananjoyo</span></h2>
											<p>Nama lengkap saya Adi Hananjoyo dan saya berumur 20 tahun, saya berasal dari Negara,dan dapat berkomunikasi dengan beberapa bahasa dengan baik, sekarang saya sedang menempuh pendidikan di prodi S1 Sistem Informasi Undiksha. Saya memiliki hobi bermain game.</p>
											<ul>
												<li><a class="a-btn-a scroll" href="#port">My Portfolio </a></li>
												<li><a class="a-btn-h scroll" href="#contact">Hire Me</a></li>
											</ul>
										</div>
					                </div>
					                  <div class="item">
					                	<div class="about-left-grid">
											<h2>Hi! Saya <span>Adi Hananjoyo</span></h2>
											<p>Nama lengkap saya Adi Hananjoyo dan saya berumur 20 tahun, saya berasal dari Negara,dan dapat berkomunikasi dengan beberapa bahasa dengan baik, sekarang saya sedang menempuh pendidikan di prodi S1 Sistem Informasi Undiksha. Saya memiliki hobi bermain game.</p>
											<ul>
												<li><a class="a-btn-a scroll" href="#port">My Work</a></li>
												<li><a class="a-btn-h scroll" href="#contact">Hire Me</a></li>
											</ul>
										</div>
					                </div>
					                  <div class="item">
					                	<div class="about-left-grid">
											<h2>Hi! Saya <span>Adi Hananjoyo</span></h2>
											<p>Nama lengkap saya Adi Hananjoyo dan saya berumur 20 tahun, saya berasal dari Negara dan dapat berkomunikasi dengan beberapa bahasa dengan baik, sekarang saya sedang menempuh pendidikan di prodi S1 Sistem Informasi Undiksha. Saya memiliki hobi bermain game.</p>
											<ul>
												<li><a class="a-btn-a scroll" href="#port">My Work</a></li>
												<li><a class="a-btn-h scroll" href="#contact">Hire Me</a></li>
											</ul>
										</div>
					                </div>
					</div>
				</div>
				<div class="col-md-6 about-right">

				</div>
				<div class="clearfix"> </div>
							<link href="css/owl.carousel.css" rel="stylesheet">
							    <script src="js/owl.carousel.js"></script>
			<script>
				$(document).ready(function() {
					$("#owl-demo1").owlCarousel({
						items : 1,
						lazyLoad : false,
						autoPlay : true,
						navigation : false,
						navigationText :  false,
						pagination : true,
					});
				});
			</script>
			<!-- Feedback -->
			<script>
				$(document).ready(function() {
					$("#owl-demo3").owlCarousel({
						items : 1,
						lazyLoad : false,
						autoPlay : true,
						navigation : false,
						navigationText :  true,
						pagination :true,
					});
				});
			</script>
			</div>
			<!-- /about -->
</body>
</html>
