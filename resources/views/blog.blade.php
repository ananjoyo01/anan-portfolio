<!DOCTYPE HTML>
<html>
<head>
<title>Anan Portfolio</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
 <!-- Custom Theme files -->
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="keywords" content="Preface Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<!-- webfonts -->
	<link href='//fonts.googleapis.com/css?family=Asap:400,700,400italic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
<!-- webfonts -->
 <!---- start-smoth-scrolling---->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
 <!---- start-smoth-scrolling---->
</head>
<body>
 <!-- top-grids -->
				<div class="blog" id="blogs">
					<div class="container">
						<div class="service-head text-center">
						<h4>BLOGS</h4>
						<h3>MY <span>BLOGS</span></h3>
						<span class="border one"></span>
					</div>
					   <div class="news-grid w3l-agile">
					    <div class="col-md-6 news-img">
						  <a href="#" data-toggle="modal" data-target="#myModal1"> <img src="images/b1.jpg" alt=" " class="img-responsive"></a>

						</div>
					    <div class="col-md-6 news-text">
						   <h3> <a href="#" data-toggle="modal" data-target="#myModal1">HERE GOES AN AWESOME BLOG TITLE</a></h3>
							<ul class="news">
								<li><i class="glyphicon glyphicon-user"></i> <a href="#">Admin</a></li>
								<li><i class="glyphicon glyphicon-comment"></i> <a href="#">2 Comments</a></li>
								<li><i class="glyphicon glyphicon-heart"></i> <a href="#">50 Likes</a></li>
								<li><i class="glyphicon glyphicon-tags"></i> <a href="#">3 Tags</a></li>
							</ul>
							<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>
							<a href="#" data-toggle="modal" data-target="#myModal1" class="read hvr-shutter-in-horizontal">Read More</a>

						</div>

						<div class="clearfix"></div>
					 </div>
					  <div class="news-grid">

					    <div class="col-md-6 news-text two">
						   <h3> <a href="#" data-toggle="modal" data-target="#myModal1">HERE GOES AN AWESOME BLOG TITLE</a></h3>
							<ul class="news">
								<li><i class="glyphicon glyphicon-user"></i> <a href="#">Admin</a></li>
								<li><i class="glyphicon glyphicon-comment"></i> <a href="#">2 Comments</a></li>
								<li><i class="glyphicon glyphicon-heart"></i> <a href="#">50 Likes</a></li>
								<li><i class="glyphicon glyphicon-tags"></i> <a href="#">3 Tags</a></li>
							</ul>
							<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>
								<a href="#" data-toggle="modal" data-target="#myModal1" class="read hvr-shutter-in-horizontal">Read More</a>

						</div>
						<div class="col-md-6 news-img two">
						   <a href="#" data-toggle="modal" data-target="#myModal1"> <img src="images/b2.jpg" alt=" " class="img-responsive"></a>

						</div>
						<div class="clearfix"></div>
					 </div>
					  <div class="news-grid">
					    <div class="col-md-6 news-img">
						  <a href="#" data-toggle="modal" data-target="#myModal1"> <img src="images/b3.jpg" alt=" " class="img-responsive"></a>

						</div>
					    <div class="col-md-6 news-text">
						   <h3> <a href="#" data-toggle="modal" data-target="#myModal1">HERE GOES AN AWESOME BLOG TITLE</a></h3>
							<ul class="news">
								<li><i class="glyphicon glyphicon-user"></i> <a href="#">Admin</a></li>
								<li><i class="glyphicon glyphicon-comment"></i> <a href="#">2 Comments</a></li>
								<li><i class="glyphicon glyphicon-heart"></i> <a href="#">50 Likes</a></li>
								<li><i class="glyphicon glyphicon-tags"></i> <a href="#">3 Tags</a></li>
							</ul>
							<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>
							<a href="#" data-toggle="modal" data-target="#myModal1" class="read hvr-shutter-in-horizontal">Read More</a>

						</div>

						<div class="clearfix"></div>
					 </div>
					</div>
				</div>
				<!-- top-grids -->
</body>
</html>
