<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\http\Controllers\HomeController::class,'index']);

Route::get('/contact', [App\http\Controllers\ContactController::class,'index']);

Route::get('/about',  [App\http\Controllers\AboutController::class,'index']);

Route::get('/services',  [App\http\Controllers\ServicesController::class,'index']);

Route::get('/achiev', [App\http\Controllers\AchievController::class,'index']);

Route::get('/blog',  [App\http\Controllers\BlogController::class,'index']);


;
